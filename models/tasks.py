from typing import Optional
from pydantic import BaseModel

class Task(BaseModel):
    kind: str = "tasks#task"
    id: Optional[str] = None
    title: str
    notes: str
    due: Optional[str] = None
    status: str = "needsAction"
    tasklist: str

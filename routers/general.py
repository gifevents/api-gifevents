from fastapi import APIRouter

router = APIRouter()


@router.get("/", tags=["general"])
async def home():
    """
    Home url
    """
    return {"msg": "Welcome to Gifs your calendar events"}

@router.get("/healthcheck", tags=["general"])
async def health():
    """
    Health check endpoint for helthy checks
    """
    return {"msg": "The api is healthy"}

from fastapi import APIRouter
from services.google import TenorClient

router = APIRouter()


@router.get("/gifs/", tags=["gifs"])
async def get_gifs(query: str):
    """
    Get top 5 gifs from Tenor based on query str
    """
    tenor = TenorClient()
    return tenor.get_gifs(query)

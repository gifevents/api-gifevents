from fastapi import APIRouter
from models.tasks import Task
from services.google import TasksClient

router = APIRouter()


@router.get("/tasks/details/{tasklist}/", tags=["tasks"])
async def get_tasks(tasklist: str):
    """
    Get Tasks from user, google auth required.
    """
    tasks = TasksClient()
    return tasks.get_tasks(tasklist)

@router.get("/tasks/lists/", tags=["tasks"])
async def get_tasklists():
    """
    Get Tasklists from user, google auth required.
    """
    tasks = TasksClient()
    return tasks.get_tasklists()

@router.post('/tasks/create/', tags=["tasks"])
async def create_task(task: Task):
    tasks = TasksClient()
    new_task = tasks.create(task)
    return new_task

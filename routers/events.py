from fastapi import APIRouter
from services.google import CalendarClient

router = APIRouter()


@router.get("/events/", tags=["events"])
async def get_events(calendar: str = 'primary'):
    """
    Get Events from user, google auth required.
    """
    events = CalendarClient()
    return events.get_events(calendar)

@router.get("/events/calendars/", tags=["events"])
async def get_calendars():
    """
    Get Calendars from user, google auth required.
    """
    events = CalendarClient()
    return events.get_calendars()

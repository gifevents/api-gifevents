from fastapi import FastAPI
from routers import (
    general,
    gifs,
    tasks,
    events
)
app = FastAPI(
    title='Gif Calendar and Tasks',
    description='Create Calendar Events and Tasks with gifs',
    version='0.2.0'
)

app.include_router(general.router)
app.include_router(gifs.router)
app.include_router(tasks.router)
app.include_router(events.router)

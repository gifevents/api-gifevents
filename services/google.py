import os
from datetime import datetime
import json
import requests

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials

from config import TENOR_API_KEY
from models.tasks import Task

LMT = 5

class TenorClient:
    def __init__(self) -> None:
        super().__init__()
        self.lmt = LMT
        self.__gkey = TENOR_API_KEY

    def get_gifs(self, query):
        r = requests.get(
            "https://g.tenor.com/v1/search?q=%s&key=%s&limit=%s" % (query, self.__gkey, self.lmt)
        )
        if r.status_code == 200:
            # load the GIFs using the urls for the smaller GIF sizes
            top_5gifs = json.loads(r.content)
        else:
            top_5gifs = None
        return top_5gifs


class TasksClient:
    def __init__(self) -> None:
        self.lmt = LMT
        self.creds = None
        self.items = []
        SCOPES = ['https://www.googleapis.com/auth/tasks']
        if os.path.exists('token.json'):
            self.creds = Credentials.from_authorized_user_file('token.json', SCOPES)

    def get_tasks(self, tasklist):
        SCOPES = ['https://www.googleapis.com/auth/tasks']
        if not self.creds:
            try:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'services/credentials/tasks_credentials.json',
                    SCOPES
                )
                self.creds = flow.run_local_server(port=50029)
            except:
                return []
            with open('token.json', 'w') as token:
                token.write(self.creds.to_json())
        with build('tasks', 'v1', credentials=self.creds) as service:
            results = service.tasks().list(tasklist=tasklist,maxResults=self.lmt).execute()
            self.items = results.get('items', [])
        return self.items

    def get_tasklists(self):
        SCOPES = ['https://www.googleapis.com/auth/tasks']
        if not self.creds:
            try:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'services/credentials/tasks_credentials.json',
                    SCOPES
                )
                self.creds = flow.run_local_server(port=50029)
            except:
                return []
            with open('token.json', 'w') as token:
                token.write(self.creds.to_json())
        with build('tasks', 'v1', credentials=self.creds) as service:
            results = service.tasklists().list(maxResults=self.lmt).execute()
            self.items = results.get('items', [])
        return self.items

    def create(self, task: Task):
        SCOPES = ['https://www.googleapis.com/auth/tasks']
        if not self.creds:
            try:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'services/credentials/tasks_credentials.json',
                    SCOPES
                )
                self.creds = flow.run_local_server(port=50029)
            except:
                return []
            with open('token.json', 'w') as token:
                token.write(self.creds.to_json())
        with build('tasks', 'v1', credentials=self.creds) as service:
            body = {
                "notes": task.notes,
                "title": task.title
            }
            results = service.tasks().insert(tasklist=task.tasklist, body=body).execute()
            self.items = results.get('items', [])
        return self.items

class CalendarClient:
    def __init__(self) -> None:
        self.lmt = LMT
        self.creds = None
        self.items = []

    def get_events(self, calendar: str = 'primary'):
        SCOPES = ['https://www.googleapis.com/auth/calendar.events']
        if not self.creds:
            try:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'services/credentials/events_credentials.json',
                    SCOPES
                )
                self.creds = flow.run_local_server(port=50029)
            except:
                return []
            with open('token_cal.json', 'w') as token:
                token.write(self.creds.to_json())
        with build('calendar', 'v3', credentials=self.creds) as service:
            results = service.events().list(
                calendarId=calendar,
                maxResults=self.lmt,
                timeMin=datetime.utcnow().isoformat() + 'Z'
            ).execute()
            self.items = results.get('items', [])
        return self.items

    def get_calendars(self):
        SCOPES = ['https://www.googleapis.com/auth/calendar.readonly']
        if not self.creds:
            try:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'services/credentials/events_credentials.json',
                    SCOPES
                )
                self.creds = flow.run_local_server(port=50029)
            except:
                return []
            with open('token_cal.json', 'w') as token:
                token.write(self.creds.to_json())
        with build('calendar', 'v3', credentials=self.creds) as service:
            results = service.calendarList().list(
                maxResults=self.lmt
            ).execute()
            self.items = results.get('items', [])
        return self.items

# Welcome to Gif Your GCalendar's API

This is the API for the project Gif Your GCalendar. A project for manage your Google calendar events and tasks throug gifs.

## How to run it locally

### Dependencies
* python ^3.9
* poetry

### install the dependencies
>>> poetry shell

>>> poetry install

### running the project
A Key from Google Tenor is required
Keys from Google API (Calendar and Tasks) are requiered

>>> uvicorn main:app --reload

Browse the docs on [127.0.0.1:8000/docs](127.0.0.1:8000/docs)

Browse the api on [127.0.0.1:8000](127.0.0.1:8000)
## TODO
[ ] UI(other repo)

[ ] Add Events

[ ] Edit Tasks

[ ] Edit Events
